Stormtest app
Managing your books
Developed by Luan Andrade - 2016 to Stormtech challenge

Tech:
- Windows 10
- Wamp
- Apache 2.4.9
- PHP 5.5.12
- Mysql 5.6.17
- CakePHP 2.8.3
- Editor: Sublime Text 2

Dir & Files:
/DB - models, scripts to create database
/screens - screenshots
/stormtest/app/Config/database.php - information to access database
/stormtest/app/Model - Models
/stormtest/app/View - View
/stormtest/app/Controller - Controllers