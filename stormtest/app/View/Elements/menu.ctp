   <!-- BEGIN MENU LAYER -->
   <div class="menu-layer">
    <ul class="">
      <li data-open-after="true" class="">
        <?php echo $this->Html->link(__('Home'), array('controller'=>'books','action' => 'index')); ?> 
        <span class="hover-bg"></span></li>
        <li class="has-child">
          <?php echo $this->Html->link(__('New Book'), array('controller'=>'books','action' => 'add')); ?> 
          <span class="hover-bg"></span>
        </li>
      </ul>
    </div><!--.menu-layer-->
    <!-- END OF MENU LAYER -->
