<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>

<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		StormTest
	</title>
	<?php
	echo $this->Html->meta('icon');

	//CSS
	//Core CSS
	echo $this->Html->css('../pleasure/admin1/css/admin1.css');
	echo $this->Html->css('../pleasure/globals/css/elements.css');
	//Plugins
	echo $this->Html->css('../pleasure/globals/css/plugins.css');

	//Scripts:
	echo $this->Html->script('../pleasure/globals/plugins/modernizr/modernizr.min.js');
	//Global and theme vendors
	echo $this->Html->script('../pleasure/globals/js/global-vendors.js');
	//pleasure
	echo $this->Html->script('../pleasure/globals/js/pleasure.js');
	//Admin
	echo $this->Html->script('../pleasure/admin1/js/layout.js');
	

	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');
	?>
	
	<!-- BEGIN INITIALIZATION-->
	<script>
	$(document).ready(function () {
		Pleasure.init();
		Layout.init();

	});
	</script>
</head>
<body>
	<?php echo $this->element('wrapper'); ?>
	<div class="content">

		<?php echo $this->Flash->render(); ?>

		<?php echo $this->fetch('content'); ?>

		<div class="footer-links margin-top-40">
			<div class="row no-gutters">
				<div class="col-xs-12 bg-grey">
					<a href="#">
						<span>StromTest - Luan 2016| <?php echo __d('cake_dev', 'CakePHP %s', Configure::version());?></span>
					</a>
				</div><!--.col-->

			</div><!--.row-->
		</div><!--.footer-links-->


	</div>
	<div class="layer-container">

		<?php
		echo $this->element('menu'); 
		?>
	</div>
	

	
</body>
</html>
