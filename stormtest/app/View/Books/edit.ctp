

<div class="page-header full-content bg-blue-grey">
	<div class="row">
		<div class="col-sm-6">
			<h1>Books <small>Manage</small></h1>
		</div><!--.col-->
		<div class="col-sm-6">
			<ol class="breadcrumb">
				<li><a href="#"><i class="ion-home"></i></a></li>
				<li><a href="#" class="active">Books</a></li>
			</ol>
		</div><!--.col-->
	</div><!--.row-->
</div><!--.page-header-->

<div class="row">
	<div class="col-md-12">
		<div class="panel">
			<?php echo $this->Form->create('Book'); ?>
			<fieldset>
				<div class="panel-heading">
					<legend><?php echo __('Edit Book'); ?></legend>
				</div>
				<div class="col-md-6">
					<?php
					echo $this->Form->input('id');
					echo $this->Form->input('title',array('class'=>'form-control','label'=>false,'placeholder'=>'Title'));
					echo $this->Form->input('author',array('class'=>'form-control','label'=>false,'placeholder'=>'Author'));
					echo $this->Form->input('edition_year',array('class'=>'form-control','label'=>false,'placeholder'=>'Year','max'=>2016,'min'=>0));
					?>
				</div>
			</fieldset>

			<?php echo $this->Form->button("Send",array('type'=>'submit','class'=>'btn btn-green btn-ripple')); ?>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</div>

