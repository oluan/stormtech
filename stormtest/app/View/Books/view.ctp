<div class="page-header full-content bg-amber">
	<div class="row">
		<div class="col-sm-6">
			<h1>Books <small>Manage</small></h1>
		</div><!--.col-->
		<div class="col-sm-6">
			<ol class="breadcrumb">
				<li><a href="#"><i class="ion-home"></i></a></li>
				<li><a href="#" class="active">Books</a></li>
			</ol>
		</div><!--.col-->
	</div><!--.row-->
</div><!--.page-header-->

<div class="row">
	<div class="col-md-12">
		<div class="panel">

			<div class="panel-heading">
				<legend><?php echo __('View Book'); ?></legend>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-3">
						<?php echo __('Id'); ?>
					</div>
					<div class="col-md-4">
						<?php echo h($book['Book']['id']); ?>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<?php echo __('Title'); ?>
					</div>
					<div class="col-md-4">
						<?php echo h($book['Book']['title']); ?>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<?php echo __('Author'); ?>
					</div>
					<div class="col-md-4">
						<?php echo h($book['Book']['author']); ?>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<?php echo __('Edition Year'); ?>
					</div>
					<div class="col-md-4">
						<?php echo h($book['Book']['edition_year']); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>