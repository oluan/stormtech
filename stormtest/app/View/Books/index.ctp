

<div class="page-header full-content bg-blue">
	<div class="row">
		<div class="col-sm-6">
			<h1>Books <small>Manage</small></h1>
		</div><!--.col-->
		<div class="col-sm-6">
			<ol class="breadcrumb">
				<li><a href="#"><i class="ion-home"></i></a></li>
				<li><a href="#" class="active">Books</a></li>
			</ol>
		</div><!--.col-->
	</div><!--.row-->
</div><!--.page-header-->

<div class="row">
	<div class="col-md-12">
		<div class="panel">
			<?php echo $this->Form->create('ordenation',array('id'=>'ordenation')); ?>
			<fieldset>
				<div class="panel-heading">
					<legend><?php echo __('Ordenation Criteria'); ?>
						<?php echo $this->Form->button("<i class='fa fa-plus'></i> criteria",array('type'=>'button','id'=>'add', 'class'=>'btn btn-primary btn-ripple','escape'=>false)); ?>
					</legend>
				</div>

				

			</fieldset>
			<?php echo $this->Form->button('Send',array('type'=>'submit','class'=>'btn btn-green btn-ripple')); ?>
			<?php echo $this->Form->end(); ?>
			<table class="table table-hover">
				<thead>
					<tr>
						<th><?php echo 'Identifier'; ?></th>
						<th><?php echo 'Title'; ?></th>
						<th><?php echo 'Author'; ?></th>
						<th><?php echo 'Edition Year'; ?></th>
						<th class="actions"><?php echo __('Actions'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($books as $book): ?>
					<tr>
						<td><?php echo "Book ".$book['books']['id']; ?>&nbsp;</td>
						<td><?php echo $book['books']['title']; ?>&nbsp;</td>
						<td><?php echo $book['books']['author']; ?>&nbsp;</td>
						<td><?php echo $book['books']['edition_year']; ?>&nbsp;</td>
						<td class="actions">
							<?php echo $this->Html->link(__("<i class='ion-android-search'></i>"), array('action' => 'view', $book['books']['id']),array('class'=>'btn btn-floating btn-teal btn-ripple','escape'=>false)); ?>
							<?php echo $this->Html->link(__("<i class='ion-android-create'></i>"), array('action' => 'edit', $book['books']['id']), array('class'=>'btn btn-floating btn-blue-grey btn-ripple','escape'=>false)); ?>
							<?php echo $this->Form->postLink(__("<i class='fa fa-trash'></i>"), array('action' => 'delete', $book['books']['id']), array('confirm' => __('Are you sure you want to delete Book # %s?', $book['books']['id']),'class'=>'btn btn-floating btn-orange btn-ripple','escape'=>false)); ?>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
</div>

<script>
var count=0;
	//add criteria selection
	$("#add").click(function(){

		var ordenation = $("#ordenation").find('fieldset');

		count++;

		ordenation.append(
			$("<div>",
			{
				id: "criteria"+count
			})
			);
		var criteria= $("#criteria"+count);

    		//small div to put the select inside
    		criteria.append("<div class='col-md-3'>");
    		
    		criteria.find('.col-md-3').append($("<select>",
    		{
    			name: "data[ordenation][criteria]["+count+"]",
    			id: "ordenationCriteria"+count,
    			class: "btn btn-default btn-ripple",
    			title:"None",
    			multiple:false
    		}));
    		
    		//add options to input select
    		$.ajax({
    			method: 'POST',
    			url: 'http://127.0.0.1/stormtest/books/get_options/',
    			dataType : "json"

    		})
    		.done(function(result)
    		{
    			console.log(result);
    			$.each(result, function(key,value) {
    				console.log('key '+key+' value '+value);
    				$('#ordenationCriteria'+count).append($("<option>").val(key).html(value));
    			});

    			//criteria.append("<div class='col-md-2 align-right'>");
    			criteria.find('.col-md-3').append(
    				$("<a>",
    				{
    					class:"btn btn-floating btn-danger btn-ripple",
    					html: "<i class='fa fa-times' ></i>",
    				})
    				.click(function()
    				{
    					$(this).parent().parent().remove();
    				})
    				);
    		});

    	});
</script>