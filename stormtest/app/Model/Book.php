<?php
App::uses('AppModel', 'Model');
/**
 * Book Model
 *
 */
class Book extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'author' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'edition_year' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//options to order the books
	private $options = array(1=>'Title Asc',2=>'Title Desc',3=>'Author Asc',4=>'Author Desc',5=>'Edition_Year Asc',6=>'Edition_Year Desc');



	//method get_options()
	// return $options
	public function get_options(){
		return $this->options;
	}

	/*method get_ordered(criteria)
	* @param criteria = array of criterias
	* @return array of books ordered. Or an empty if there is no criteria
	*/
	public function get_ordered($criteria = array()){
		
		if (empty($criteria)) return array();

		$order = array();
		foreach ($criteria['ordenation']['criteria'] as $key => $value) {
			array_push($order, $this->options[$value]);
		}
		$query = "select * from books order by ".implode(',',$order);
		
		return $this->query($query);
	}
}
